<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\FightArena;
use App\Task1\Fighter;

class FightArenaHtmlPresenter
{
    /**
     * @param FightArena $arena
     * @return string
     */
    public function present(FightArena $arena): string
    {
        $html = "<div>";

        /** @var Fighter $fighter */
        foreach ($arena->all() as $fighter) {
            $html .= $fighter->getName() .': ' . $fighter->getAttack() . ', ' . $fighter->getHealth() . '<img src="' . $fighter->getImage() . '">';
        }

        $html .= "</div>";

        return $html;
    }
}
