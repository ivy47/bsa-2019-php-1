<?php

declare(strict_types=1);

namespace App\Task2;

class EmojiGenerator
{
    /** @var array  */
    private $emojis = ['🚀', '🚃', '🚄', '🚅', '🚇'];

    /**
     * @return \Generator
     */
    public function generate(): \Generator
    {
        foreach ($this->emojis as $emoji) {
            yield $emoji;
        }
    }
}
