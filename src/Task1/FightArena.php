<?php

declare(strict_types=1);

namespace App\Task1;

class FightArena
{
    /** @var Fighter[] */
    private $fighters = [];

    /**
     * @param Fighter $fighter
     */
    public function add(Fighter $fighter): void
    {
        $this->fighters[] = $fighter;
    }

    /**
     * @return Fighter
     */
    public function mostPowerful(): Fighter
    {
        $maxValue = 0;
        $key = null;

        /** @var Fighter $fighter */
        foreach ($this->fighters as $k => $fighter) {
            if ($fighter->getAttack() > $maxValue) {
                $maxValue = $fighter->getAttack();
                $key = $k;
            }
        }

        return $this->fighters[$key];
    }

    /**
     * @return Fighter
     */
    public function mostHealthy(): Fighter
    {
        $maxValue = 0;
        $key = null;

        /** @var Fighter $fighter */
        foreach ($this->fighters as $k => $fighter) {
            if ($fighter->getHealth() > $maxValue) {
                $maxValue = $fighter->getHealth();
                $key = $k;
            }
        }

        return $this->fighters[$key];
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return $this->fighters;
    }
}
